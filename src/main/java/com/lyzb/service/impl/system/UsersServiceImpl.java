package com.lyzb.service.impl.system;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lyzb.dao.domain.system.User;
import com.lyzb.dao.system.IUsersDao;
import com.lyzb.mapper.IUserMapper;
import com.lyzb.service.system.IUsersService;


  
@Service(value="usersService")  
@Transactional(rollbackFor=Exception.class)  
public class UsersServiceImpl implements IUsersService {  
      
	@Autowired  
    private IUserMapper userMapper;  
      
	@Autowired 
    private IUsersDao usersDao;  
      
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)  
    public User queryUsersById(int id){  
        return userMapper.queryUserById(id);  
    }  
      
    private static final Log logger = 
    		LogFactory.getLog(UsersServiceImpl.class);
    
   /* *//**
     * 使用dao方式
     *//*
    @SuppressWarnings("rawtypes")
    @Transactional(propagation=Propagation.REQUIRED, readOnly=true)  
    public User queryUsersByName(String name){  
        List list = usersDao.queryUserByName(name);  
        System.out.println("list = "+list==null?"0":list.size());  
        if(list!=null&&list.size()>0) return (User)list.get(0);  
        return null;  
    }  
      
      
    */
    /**
     * 使用dao方式
     */
    @Transactional(propagation=Propagation.REQUIRED)  
    public Integer saveUser(User user){  
    	Integer result = 0;
        if(user.getUserId()<=0){
        	logger.info("insertUser");
            result = usersDao.saveUser(user);
        }else{  
        	logger.info("updateUser");  
            //result  = userMapper.updateUser(user);  
        }         
        //throw new RuntimeException("@@ Rollback for Debug..........");          
		return result;
    }

	@Override
	public User queryUserByName(String userName) {
		return usersDao.getUserByUserName(userName);
	}  
} 
