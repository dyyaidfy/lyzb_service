package com.lyzb.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.lyzb.dao.IGoodsDao;
import com.lyzb.dao.domain.Goods;
import com.lyzb.service.IGoodsService;


  
@Service(value="goodsService")  
@Transactional(rollbackFor=Exception.class)  
public class GoodsServiceImpl implements IGoodsService {  
    private static final Log logger =LogFactory.getLog(GoodsServiceImpl.class);
      
	@Autowired 
    private IGoodsDao goodsDao;  

    @Override
    @Transactional(propagation=Propagation.REQUIRED)  
	public Integer modifyGoods(Goods goods) {
    	logger.error("services modify goods ");
    	return goodsDao.modifyGoods(goods);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)  
	public Integer saveGoods(Goods goods) {
		logger.error("services save goods ");
		return goodsDao.saveGoods(goods);
	}
} 
