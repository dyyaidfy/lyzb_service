package com.lyzb.service;

import com.lyzb.dao.domain.Goods;

public interface IGoodsService {  
	Integer modifyGoods(Goods goods);
	Integer saveGoods(Goods goods);
}
