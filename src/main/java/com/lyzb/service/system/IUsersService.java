package com.lyzb.service.system;

import com.lyzb.dao.domain.system.User;

public interface IUsersService {  
    public  Integer saveUser(User user);  
    public  User  queryUserByName(String userName);
}
